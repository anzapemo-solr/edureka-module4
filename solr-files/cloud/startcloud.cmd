:: Runs solr cloud

:: Stop all
call ..\bin\solr.cmd stop -all

:: First run the node 1
call ..\bin\solr.cmd start -c -noprompt -s "node1" -p 8983

:: Then run the node 2
call ..\bin\solr.cmd start -c -noprompt -s "node2" -p 8984 -z localhost:9983

:: Then create the configuration in zookeeper
call ..\bin\solr.cmd zk upconfig -n edureka_coll_config -d "conf" -z localhost:9983

:: Create the collection
call ..\bin\solr.cmd create -c movie_catalog -n edureka_coll_config -d conf -p 8983 -s 2 -rf 2