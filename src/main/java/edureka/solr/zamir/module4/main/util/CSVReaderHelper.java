package edureka.solr.zamir.module4.main.util;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import com.opencsv.CSVReader;

/**
 * Reads CSV files and returns an ArrayList<String[]>
 * 
 * @author Zamir
 *
 */
public class CSVReaderHelper {

	/**
	 * Read a no-header CSV
	 * 
	 * @param path
	 * @return
	 */
	public static ArrayList<String[]> readCSV(String path) throws Exception {
		ArrayList<String[]> lines = new ArrayList<String[]>();
		Reader reader = Files.newBufferedReader(Paths.get(path));

		CSVReader csvReader = new CSVReader(reader);

		lines = (ArrayList<String[]>) csvReader.readAll();

		csvReader.close();
		reader.close();
		return lines;
	}

}
