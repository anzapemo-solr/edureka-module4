package edureka.solr.zamir.module4.main.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.SpellCheckResponse.Suggestion;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

/**
 * Helper for JSON operations
 * 
 * @author Zamir
 *
 */
public class JSON {

	/**
	 * Converts an Array<String> to a json array that belongs to a property
	 * given objectName
	 * 
	 * @param objectName
	 * @param items
	 * @return
	 */
	public static JsonObject arrayListToJsonArray(String objectName, ArrayList<String> items) {
		JsonObject object = new JsonObject();

		Gson gson = new Gson();
		JsonElement element = gson.toJsonTree(items, new TypeToken<ArrayList<String>>() {
		}.getType());
		object.add(objectName, element);
		return object;
	}

	/**
	 * Converts an array string e.g "[1, 2, 3]" into a json array that belongs
	 * to a property given objectName
	 * 
	 * @param objectName
	 * @param items
	 * @return
	 */
	public static JsonObject arrayStringToJsonArray(String objectName, String stringArray) {
		JsonObject object = new JsonObject();

		Gson gson = new Gson();
		JsonElement element = gson.toJsonTree(stringArray, new TypeToken<String>() {
		}.getType());
		object.add(objectName, element);
		return object;
	}
	
	/**
	 * Returns a JsonObject iterating a list of counts
	 * @param counts
	 * @return
	 */
	public static JsonObject listCountRToJsonObject(List<org.apache.solr.client.solrj.response.RangeFacet.Count> counts) {
		JsonObject object = new JsonObject();
		
		if (counts == null || counts.size() == 0 ) {
			return null;
		}
		
		for (org.apache.solr.client.solrj.response.RangeFacet.Count count : counts) {
			object.addProperty(count.getValue(), count.getCount());
		}
		return object;
	}
	
	/**
	 * Returns a JsonObject iterating a list of counts
	 * @param counts
	 * @return
	 */
	public static JsonObject listCountFToJsonObject(List<org.apache.solr.client.solrj.response.FacetField.Count> counts) {
		JsonObject object = new JsonObject();
		
		if (counts == null || counts.size() == 0 ) {
			return null;
		}
		
		for (Count count : counts) {
			object.addProperty(count.getName(), count.getCount());
		}
		return object;
	}
	
	/**
	 * Convert a list of suggestion (spellings) to a json object
	 * @param suggestions
	 * @return
	 */
	public static JsonObject spellingsListToJsonObject(List<Suggestion> suggestions) {
		JsonObject object = new JsonObject();
		
		if (suggestions == null || suggestions.size() == 0 ) {
			return null;
		}
		
		for (Suggestion suggestion : suggestions) {
			for (String alt : suggestion.getAlternatives()) {
				object.addProperty(suggestion.getToken(), alt);
			}
		}
		return object;
	}

}
