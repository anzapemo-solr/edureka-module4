package edureka.solr.zamir.module4.main;

import static spark.Spark.get;
import static spark.Spark.staticFileLocation;

import com.google.gson.JsonObject;

import edureka.solr.zamir.module4.main.util.JSON;
import edureka.solr.zamir.module4.solr.client.SolrClientHelper;
import edureka.solr.zamir.module4.solr.client.WebResponse;

/**
 * This class acts as the server for UI
 * 
 * @author Zamir
 *
 */
public class Server {

	/**
	 * Initialize the server
	 */
	@SuppressWarnings("unchecked")
	public static void initServer() {
		// For static content
		staticFileLocation("/static");

		// Redirect when "/" is requested
		get("/", (request, response) -> {
			response.redirect("/index");
			return null;
		});

		get("/suggest/:text", (request, response) -> {
			// Get the text for suggesting
			String textToSuggest = request.params(":text");

			if (textToSuggest != null && textToSuggest.length() > 0) {
				try {
					// Construct the array
					JsonObject object = JSON.arrayListToJsonArray("suggestions",
							SolrClientHelper.searchForSuggestions(textToSuggest));
					return object.toString();
				} catch (Exception e) {
					e.printStackTrace();
					return null;
				}

			}
			return null;
		});

		get("/search/:text/:page/:field", (request, response) -> {
			// Get the text for searching
			String textToSearch = request.params(":text");
			String field = request.params(":field");
			int page = Integer.parseInt(request.params(":page"));
			if (textToSearch != null && textToSearch.length() > 0) {
				try {
					WebResponse wr;
					if (!field.equals("null")) {
						wr = SolrClientHelper.search(field, textToSearch, page);
					} else {
						wr = SolrClientHelper.webSearch(textToSearch, page);
					}

					JsonObject object = JSON.arrayStringToJsonArray("results", wr.getJsonResponse());
					object.addProperty("numFound", wr.getNumFound());
					return object.toString();
				} catch (Exception e) {
					e.printStackTrace();
					return null;
				}

			}
			return null;
		});

		get("/facets/:field", (request, response) -> {
			// Get the text for searching
			String facetField = request.params(":field");
			try {
				// Construct the array
				if (facetField.equals("vote_count")) {

					return JSON
							.listCountRToJsonObject(
									(SolrClientHelper.searchRangeFacet(facetField, 10, 10, 50).get(0).getCounts()))
							.toString();
				} else {
					return JSON.listCountFToJsonObject((SolrClientHelper.searchFacet(facetField))).toString();
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}

		});

		get("/spell/:text", (request, response) -> {
			// Get the text for spelling
			String spellField = request.params(":text");
			try {
				return JSON.spellingsListToJsonObject(SolrClientHelper.searchSpellings(spellField));
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		});
	}

}
