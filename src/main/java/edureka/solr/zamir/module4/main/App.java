package edureka.solr.zamir.module4.main;

import java.util.ArrayList;

import edureka.solr.zamir.module4.main.util.CSVReaderHelper;
import edureka.solr.zamir.module4.solr.client.DocumentFactory;
import edureka.solr.zamir.module4.solr.client.QuerySet;
import edureka.solr.zamir.module4.solr.client.SolrClientHelper;
import edureka.solr.zamir.module4.solr.schema.SchemaBuilder;

/**
 * Entry class Please read the instructions.MD file first This program assumes
 * that an instance is up and files and folders are correctly placed (see
 * instructions.MD)
 * 
 * @author Zamir
 *
 */
public class App {

	/**
	 * Core's name
	 */
	public static final String CORE_NAME = "movie_catalog";

	public static void main(String[] args) {

		// First create the core.
		try {
//			SolrClientHelper.createCore(CORE_NAME); // The collection is already created within the solr-files/cloud/startcloud.cmd
			System.out.println("Core created");
		} catch (Exception e) {
			System.out.println("Impossible to create core, may be already exists: " + e.getMessage());
		}

		System.out.println("Going on ...");

		// Then, create the field types
//		SchemaBuilder.addFieldTypes(); // Moved to schema.xml due to suggester initialization requires it
		// Then, create the fields
		SchemaBuilder.addFields();

		// Read the file and index the docs
		try {
			ArrayList<String[]> moviesRaw = CSVReaderHelper.readCSV("movies.csv");
			SolrClientHelper.indexDocuments(DocumentFactory.createDocuments(moviesRaw));
			System.out.println("Documents created!");
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Do the queries!
		// First add the queries to an array for best understanding
		ArrayList<String> queryList = new ArrayList<String>();
		queryList.add(QuerySet.QUERY_BY_TITLE);
		queryList.add(QuerySet.QUERY_BY_KEYWORDS);
		queryList.add(QuerySet.QUERY_BY_COUNTRY);
		queryList.add(QuerySet.QUERY_BY_POPULARITY);
		queryList.add(QuerySet.QUERY_BY_VOTE_COUNT);
		queryList.add(QuerySet.QUERY_BY_RUN_TIME);
		queryList.add(QuerySet.QUERY_BY_KEY_WORDS_X_OR_Y_PRESENT_Z_NO_PRESENT);
		queryList.add(QuerySet.QUERY_BY_TITLE_AND_KEYWORD);
		queryList.add(QuerySet.QUERY_BY_TITLE_FIRST_AND_KEYWORD);
		queryList.add(QuerySet.QUERY_BY_RUNTIME_LESS_THAN);
		queryList.add(QuerySet.QUERY_BY_RUNTIME_BETWEEN);
		queryList.add(QuerySet.QUERY_BY_FIGHT_PRECEDENCE_MAN);
		// Then loop the array and make the query
		for (String query : queryList) {
			try {
//				SolrClientHelper.printSolrDocuments("Query: " + query, SolrClientHelper.query(query));
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Error running query: " + query);
			}
		}
		
		//Initialize the server to serve suggesting/search functionality
		Server.initServer();

	}
}
