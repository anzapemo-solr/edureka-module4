package edureka.solr.zamir.module4.solr.client;

/**
 * This interface holds the query requested in the case study
 * 
 * @author Zamir
 *
 */
public interface QuerySet {

	public static final String QUERY_BY_TITLE = "title:Movie 1";
	public static final String QUERY_BY_KEYWORDS = "keywords:Tears+Women"; //Multivalue
	public static final String QUERY_BY_COUNTRY = "production_country:Argentina";
	public static final String QUERY_BY_POPULARITY = "popularity:[90 TO *]"; //Greater than 90
	public static final String QUERY_BY_VOTE_COUNT = "vote_count:[* TO 50]"; //Less or equal than 50
	public static final String QUERY_BY_RUN_TIME = "run_time:[60 TO *]"; //More than 1 hour
	public static final String QUERY_BY_KEY_WORDS_X_OR_Y_PRESENT_Z_NO_PRESENT = "keywords:(+terror +horror -movie1)"; //Search for terror and horror but not movie1
	public static final String QUERY_BY_TITLE_AND_KEYWORD = "(title:\"movie 4\" keywords:Women)";
	public static final String QUERY_BY_TITLE_FIRST_AND_KEYWORD = "(title:\"movie 3\"^2 keywords:(horror OR hot))"; //Boosting title and searching by keywords with OR
	public static final String QUERY_BY_RUNTIME_LESS_THAN = "run_time:[* TO 120]"; //Less than 2 hours
	public static final String QUERY_BY_RUNTIME_BETWEEN = "run_time:{30 TO 60}"; //Between 30 and 60 (exclusive)
	public static final String QUERY_BY_FIGHT_PRECEDENCE_MAN = "(title:fight)^2 OR (title:man)"; //Title fight or man, with precedence of fight

}
