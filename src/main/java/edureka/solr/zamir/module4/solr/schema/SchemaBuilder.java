package edureka.solr.zamir.module4.solr.schema;

import java.io.UnsupportedEncodingException;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import edureka.solr.zamir.module4.main.App;
import edureka.solr.zamir.module4.solr.client.SolrClientHelper;

/**
 * This class helps to construct via POST petitions the desired schema
 * 
 * @author Zamir
 *
 */
public class SchemaBuilder {

	private static HttpClient client;
	private static JsonParser parser;

	public static final String SCHEMA_URL = SolrClientHelper.HOST_URL + App.CORE_NAME + "/schema";

	static {
		resetClient();
		parser = new JsonParser();
	}

	/**
	 * Makes a post request to the specified URL and a Json string
	 * 
	 * @param url
	 * @throws UnsupportedEncodingException
	 */
	public static void makeJsonPostRequest(String url, String json) throws Exception {
		resetClient();
		HttpPost post = new HttpPost(url);
		post.setHeader("Accept", "application/json");
		post.setHeader("Content-Type", "application/json");
		StringEntity entity = new StringEntity(parseStringJson(json));
		post.setEntity(entity);
		client.execute(post);		
	}
	
	/**
	 * Re creates the client
	 */
	private static void resetClient() {
		client = HttpClientBuilder.create().build();
	}

	/**
	 * Add the field types to the schema
	 */
	public static void addFieldTypes() {
//		{
//			"add-field-type": {
//				"name": "text_search",
//				"class": "solr.StrField",
//				"analyzer": {
//					"tokenizer": {
//						"class": "solr.StandardTokenizerFactory"
//					},
//					"filters": [
//						{
//							"class": "solr.LowerCaseFilterFactory"
//						}
//					]
//				}
//			}
//		}
		String textSearchDef = "{\"add-field-type\": {\"name\": \"text_search\",\"class\": \"solr.TextField\",\"analyzer\": {\"tokenizer\": {\"class\": \"solr.StandardTokenizerFactory\"},\"filters\": [{\"class\": \"solr.LowerCaseFilterFactory\"}]}}}";
		try {
			makeJsonPostRequest(SCHEMA_URL, textSearchDef);
			System.out.println("text_search field type created");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error creating the text_search field type.");
			System.exit(1);
		}
	}
	
	/**
	 * Add fields to the schema
	 */
	public static void addFields() {
//		{
//			"add-field": {
//				"name": "title",
//				"type": "text_search",
//				"stored": true,
//				"indexed": true,
//				"required": true,
//				"multiValued": false
//			}
//		}
		String titleFieldDef = "{\"add-field\": {\"name\": \"title\",	\"type\": \"text_search\",\"stored\": true,\"indexed\": true,\"required\": true,\"multiValued\": false}}";
		
		//		{
		//		"add-field": {
		//			"name": "production_company",
		//			"type": "text_search",
		//			"stored": true,
		//			"indexed": true,
		//			"required": true,
		//			"multiValued": false
		//		}
		//	}
		String productionCompanyFieldDef = "{\"add-field\": {\"name\": \"production_company\",	\"type\": \"text_search\",\"stored\": true,\"indexed\": true,\"required\": true,\"multiValued\": false}}";
	
		//	{
		//	"add-field": {
		//		"name": "production_country",
		//		"type": "text_search",
		//		"stored": true,
		//		"indexed": true,
		//		"required": true,
		//		"multiValued": false
		//	}
		//}
		String productionCountryFieldDef = "{\"add-field\": {\"name\": \"production_country\",	\"type\": \"text_search\",\"stored\": true,\"indexed\": true,\"required\": true,\"multiValued\": false}}";

		//{
		//"add-field": {
		//	"name": "tagline",
		//	"type": "text_search",
		//	"stored": true,
		//	"indexed": true,
		//	"required": true,
		//	"multiValued": false
		//}
		//}
		String tagLineFieldDef = "{\"add-field\": {\"name\": \"tagline\",	\"type\": \"text_search\",\"stored\": true,\"indexed\": true,\"required\": true,\"multiValued\": false}}";

		//{
		//"add-field": {
		//	"name": "genre",
		//	"type": "text_search",
		//	"stored": true,
		//	"indexed": true,
		//	"required": true,
		//	"multiValued": false
		//}
		//}
		String genreFieldDef = "{\"add-field\": {\"name\": \"genre\",	\"type\": \"text_search\",\"stored\": true,\"indexed\": true,\"required\": true,\"multiValued\": false}}";
		
		//{
		//"add-field": {
		//	"name": "original_languages",
		//	"type": "text_search",
		//	"stored": true,
		//	"indexed": true,
		//	"required": true,
		//	"multiValued": true
		//}
		//}
		String originalLanguagesFieldDef = "{\"add-field\": {\"name\": \"original_languages\",	\"type\": \"text_search\",\"stored\": true,\"indexed\": true,\"required\": true,\"multiValued\": true}}";
		
		//{
		//"add-field": {
		//	"name": "keywords",
		//	"type": "text_search",
		//	"stored": true,
		//	"indexed": true,
		//	"required": true,
		//	"multiValued": true
		//}
		//}
		String keywordsFieldDef = "{\"add-field\": {\"name\": \"keywords\",	\"type\": \"text_search\",\"stored\": true,\"indexed\": true,\"required\": true,\"multiValued\": true}}";
		
		//{
		//"add-field": {
		//	"name": "run_time",
		//	"type": "pint",
		//	"stored": true,
		//	"indexed": true,
		//	"required": true,
		//	"multiValued": false
		//}
		//}
		String runTimeFieldDef = "{\"add-field\": {\"name\": \"run_time\",	\"type\": \"text_search\",\"stored\": true,\"indexed\": true,\"required\": true,\"multiValued\": false}}";

		//{
		//"add-field": {
		//	"name": "popularity",
		//	"type": "pint",
		//	"stored": true,
		//	"indexed": true,
		//	"required": true,
		//	"multiValued": false
		//}
		//}
		String popularityFieldDef = "{\"add-field\": {\"name\": \"popularity\",	\"type\": \"text_search\",\"stored\": true,\"indexed\": true,\"required\": true,\"multiValued\": false}}";

		//{
		//"add-field": {
		//	"name": "vote_count",
		//	"type": "pint",
		//	"stored": true,
		//	"indexed": true,
		//	"required": true,
		//	"multiValued": false
		//}
		//}
		String voteCountFieldDef = "{\"add-field\": {\"name\": \"vote_count\",	\"type\": \"pint\",\"stored\": true,\"indexed\": true,\"required\": true,\"multiValued\": false}}";

		//{
		//"add-field": {
		//	"name": "budget",
		//	"type": "pfloat",
		//	"stored": true,
		//	"indexed": true,
		//	"required": true,
		//	"multiValued": false
		//}
		//}
		String budgetFieldDef = "{\"add-field\": {\"name\": \"budget\",	\"type\": \"text_search\",\"stored\": true,\"indexed\": true,\"required\": true,\"multiValued\": false}}";

		//{
		//"add-field": {
		//	"name": "vote_average",
		//	"type": "pfloat",
		//	"stored": true,
		//	"indexed": true,
		//	"required": true,
		//	"multiValued": false
		//}
		//}
		String voteAverageFieldDef = "{\"add-field\": {\"name\": \"vote_average\",	\"type\": \"text_search\",\"stored\": true,\"indexed\": true,\"required\": true,\"multiValued\": false}}";
		
		
		try {
			makeJsonPostRequest(SCHEMA_URL, titleFieldDef);
			makeJsonPostRequest(SCHEMA_URL, productionCompanyFieldDef);
			makeJsonPostRequest(SCHEMA_URL, productionCountryFieldDef);
			makeJsonPostRequest(SCHEMA_URL, tagLineFieldDef);
			makeJsonPostRequest(SCHEMA_URL, genreFieldDef);
			makeJsonPostRequest(SCHEMA_URL, originalLanguagesFieldDef);
			makeJsonPostRequest(SCHEMA_URL, keywordsFieldDef);
			makeJsonPostRequest(SCHEMA_URL, runTimeFieldDef);
			makeJsonPostRequest(SCHEMA_URL, popularityFieldDef);
			makeJsonPostRequest(SCHEMA_URL, voteCountFieldDef);
			makeJsonPostRequest(SCHEMA_URL, budgetFieldDef);
			makeJsonPostRequest(SCHEMA_URL, voteAverageFieldDef);
			addWebSearchField();
			System.out.println("Fields created");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error creating the fields.");
			System.exit(1);
		}
		
}
	
	/**
	 * Adds the field for web_search (suggestion)
	 * Adds the copy fields rules:
	 * 1. Copy title to web_search
	 * 2. Copy keywords to web_search
	 */
	public static void addWebSearchField(){
		//{"add-field": {
		//	"name": "web_search",
		//	"type": "text_search",
		//	"stored": true,
		//	"indexed": true,
		//	"required": false,
		//	"multiValued": true
		//}
		//}
		String webSearchFieldDef = "{\"add-field\": {\"name\": \"web_search\",	\"type\": \"text_search\",\"stored\": true,\"indexed\": true,\"required\": false,\"multiValued\": true}}";
		
//		{"add-copy-field": {
//			"source": "title",
//			"dest" : "web_search"
//		}
//		}
		String titleCopyFieldDef = "{\"add-copy-field\": {\"source\": \"title\",	\"dest\": \"web_search\"}}";
		
//		{"add-copy-field": {
//			"source": "title",
//			"dest" : "web_search"
//		}
//		}
		String keywordsCopyFieldDef = "{\"add-copy-field\": {\"source\": \"keywords\",	\"dest\": \"web_search\"}}";
		try {
			makeJsonPostRequest(SCHEMA_URL, webSearchFieldDef);
			makeJsonPostRequest(SCHEMA_URL, titleCopyFieldDef);
			makeJsonPostRequest(SCHEMA_URL, keywordsCopyFieldDef);
			System.out.println("Field web_search created");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error creating the web_search field");
			System.exit(1);
		}
	}

	/**
	 * Parse a json string to a valid JSon String
	 * 
	 * @param json
	 * @return valid json string
	 */
	public static String parseStringJson(String json) {
		JsonElement jsonElement = parser.parse(json);
		JsonObject jsonObject = jsonElement.getAsJsonObject();
		return jsonObject.toString();
	}

}
