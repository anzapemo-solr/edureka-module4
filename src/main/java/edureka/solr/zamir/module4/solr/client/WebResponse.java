package edureka.solr.zamir.module4.solr.client;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;

import com.google.gson.Gson;

/**
 * Acts as wrapper for serving the web response
 * 
 * @author zamir.pena
 *
 */
public class WebResponse {

	private long numFound;
	private String jsonResponse;
	private QueryResponse queryResponse;
	private Gson gson = new Gson();

	public WebResponse(QueryResponse solrResponse) {
		this.queryResponse = solrResponse;
		this.jsonResponse = gson.toJson(getDocuments());
		this.numFound = getDocuments().getNumFound();
	}

	public QueryResponse getQueryResponse() {
		return queryResponse;
	}

	public void setQueryResponse(QueryResponse queryResponse) {
		this.queryResponse = queryResponse;
	}

	public SolrDocumentList getDocuments() {
		return this.queryResponse.getResults();
	}

	public long getNumFound() {
		return numFound;
	}

	public void setNumFound(long numFound) {
		this.numFound = numFound;
	}

	public String getJsonResponse() {
		return jsonResponse;
	}

	public void setJsonResponse(String jsonResponse) {
		this.jsonResponse = jsonResponse;
	}

}
