package edureka.solr.zamir.module4.solr.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.SolrInputField;

/**
 * Creates SolrInputDocuments
 * 
 * @author Zamir
 *
 */
public class DocumentFactory {

	/**
	 * Creates a {@link SolrInputDocument} based on a String[] 
	 * Having String[#]:
	 * #0  id
	 * #1  title 
	 * #2  production_company 
	 * #3  production_country 
	 * #4  tagline 
	 * #5  genre 
	 * #6  original_languages  -> multivalue
	 * #7  keywords  -> multivalue
	 * #8  run_time 
	 * #9  popularity 
	 * #10 vote_count 
	 * #11 budget 
	 * #12 vote_average
	 * 
	 * @param values
	 * @return
	 */
	private static SolrInputDocument createDocument(String[] values) {
		Map<String, SolrInputField> fields = new HashMap<String, SolrInputField>();
		fields.put("id", createSolrField("id", values[0]));
		fields.put("title", createSolrField("title", values[1]));
		fields.put("production_company", createSolrField("production_company", values[2]));
		fields.put("production_country", createSolrField("production_country", values[3]));
		fields.put("tagline", createSolrField("tagline", values[4]));
		fields.put("genre", createSolrField("genre", values[5]));
		fields.put("original_languages", createSolrField("original_languages", values[6].split(";")));
		fields.put("keywords", createSolrField("keywords", values[7].split(";")));
		fields.put("run_time", createSolrField("run_time", values[8]));
		fields.put("popularity", createSolrField("popularity", values[9]));
		fields.put("vote_count", createSolrField("vote_count", values[10]));
		fields.put("budget", createSolrField("budget", values[11]));
		fields.put("vote_average", createSolrField("vote_average", values[12]));
		SolrInputDocument doc = new SolrInputDocument(fields);
		return doc;
	}
	
	/**
	 * Creates an {@link ArrayList} of {@link SolrInputDocument} given an ArrayList<String[]>
	 * @param rawValues
	 * @return
	 */
	public static ArrayList<SolrInputDocument> createDocuments(ArrayList<String[]> rawValues) {
		ArrayList<SolrInputDocument> docs = new ArrayList<SolrInputDocument>();
		for (String[] values : rawValues){
			docs.add(createDocument(values));
		}
		return docs;
	}

	/**
	 * Creates a Solr field
	 * 
	 * @param name
	 *            Field name
	 * @param value
	 *            String or array for multivalues
	 * @return SolrInputField
	 */
	private static SolrInputField createSolrField(String name, Object value) {
		SolrInputField field = new SolrInputField(name);
		field.setValue(value);
		return field;
	}

}
