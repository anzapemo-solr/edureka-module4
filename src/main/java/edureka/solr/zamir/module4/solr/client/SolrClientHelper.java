package edureka.solr.zamir.module4.solr.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.request.GenericSolrRequest;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.RangeFacet;
import org.apache.solr.client.solrj.response.SpellCheckResponse;
import org.apache.solr.client.solrj.response.SpellCheckResponse.Suggestion;
import org.apache.solr.client.solrj.response.SuggesterResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.MapSolrParams;
import org.apache.solr.common.params.SolrParams;

import edureka.solr.zamir.module4.main.App;

/**
 * Helper for all the operations
 * 
 * @author Zamir
 *
 */
public class SolrClientHelper {

	/**
	 * Create a SolrJ client
	 */
	private static SolrClient client;
	/**
	 * Another client for index documents
	 */
	private static SolrClient searchClient;
	/**
	 * Another client for saerch spellings
	 */
	private static SolrClient spellClient;

	public static final String HOST_URL = "http://localhost:8983/solr/";

	private static boolean spellIndexBuilt = false;

	private static List<String> zkHosts = new ArrayList<String>();
	/**
	 * Initialization
	 */
	static {
		zkHosts.add("localhost:9983");
		initializeClients();
	}

	public static void initializeClients() {
		client = new CloudSolrClient.Builder(zkHosts, Optional.empty()).build();
		searchClient = new CloudSolrClient.Builder(zkHosts, Optional.empty()).build();
		((CloudSolrClient) client).setDefaultCollection(App.CORE_NAME);
		((CloudSolrClient) searchClient).setDefaultCollection(App.CORE_NAME);
		resetSpellClient();
	}

	public static void resetSpellClient() {
		spellClient = new CloudSolrClient.Builder(zkHosts, Optional.empty()).build();
		((CloudSolrClient) spellClient).setDefaultCollection(App.CORE_NAME);
	}

	/**
	 * Makes a request against Solr'
	 * 
	 * @param mapParams
	 *            Map of params for the request
	 * @param method
	 *            REST method
	 * @param path
	 *            URL path for client
	 * @throws Exception
	 */
	public static void makeRequest(Map<String, String> mapParams, SolrRequest.METHOD method, String path)
			throws Exception {
		SolrParams params = new MapSolrParams(mapParams);
		GenericSolrRequest req = new GenericSolrRequest(SolrRequest.METHOD.POST, path, params);
		client.request(req);
	}

	/**
	 * Method for create the core Example:
	 * /admin/cores?action=CREATE&name=movie_catalog&instanceDir=movie_catalog&config=solrconfig.xml&dataDir=data
	 * 
	 * @throws Exception
	 */
	public static void createCore(String coreName) throws Exception {
		Map<String, String> mapParams = new HashMap<String, String>();
		mapParams.put("action", "CREATE");
		mapParams.put("name", coreName);
		mapParams.put("instanceDir", coreName);
		mapParams.put("config", "solrconfig.xml");
		mapParams.put("dataDir", "data");
		makeRequest(mapParams, SolrRequest.METHOD.POST, "/admin/cores");
	}

	/**
	 * Adds a list of SolrInputDocument to the index
	 * 
	 * @param docs
	 * @throws Exception
	 */
	public static void indexDocuments(ArrayList<SolrInputDocument> docs) throws Exception {

		int counter = 0;
		for (SolrInputDocument doc : docs) {
			try {
				client.add(App.CORE_NAME, doc);

				counter++;
				// Each 100 docs, commit.
				if (counter % 100 == 0) {
					client.commit(App.CORE_NAME);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		client.commit(App.CORE_NAME);
	}

	/**
	 * Makes a query to /select RequestHandler
	 * 
	 * @param query
	 *            The query String
	 * @return {@link SolrDocumentList}
	 * @throws Exception
	 */
	public static SolrDocumentList query(String query) throws Exception {
		SolrQuery solrQuery = new SolrQuery();
		solrQuery.setRequestHandler("/select");
		solrQuery.setQuery(query);

		QueryResponse response = searchClient.query(solrQuery);
		return response.getResults();
	}

	/**
	 * Prints a list of {@link SolrDocument} ( {@link SolrDocumentList} )
	 * 
	 * @param header
	 *            Optional header
	 * @param docs
	 *            List of docs
	 */
	public static void printSolrDocuments(String header, SolrDocumentList docs) {
		System.out.println("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		System.out.println(header);
		for (SolrDocument doc : docs) {
			System.out.println(doc.toString());
		}
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
	}

	/**
	 * Search for suggestions and return them given the text
	 * 
	 * @param text
	 * @return ArrayList<String> of terms
	 * @throws SolrServerException
	 * @throws IOException
	 */
	public static ArrayList<String> searchForSuggestions(String text) throws SolrServerException, IOException {
		SolrQuery solrQuery = new SolrQuery();
		solrQuery.setRequestHandler("/suggest");
		solrQuery.set("suggest", "true");
		solrQuery.set("suggest.q", text);
		solrQuery.set("suggest.dictionary", "edurekaSuggester");

		// Call the suggester
		SuggesterResponse sResponse = searchClient.query(solrQuery).getSuggesterResponse();
		// Get suggestions
		Map<String, List<String>> termsMap = sResponse.getSuggestedTerms();
		List<String> terms = termsMap.get("edurekaSuggester");
		// Deduplicate'em
		ArrayList<String> finalTerms = new ArrayList<String>();
		for (String term : terms) {
			if (!finalTerms.contains(term)) {
				finalTerms.add(term);
			}
		}
		return finalTerms;
	}

	/**
	 * Returns a resulting JSON from a /select search to Solr' Only 3 results are
	 * returned per page unless the parameter be 0
	 * 
	 * @param field
	 * @param search
	 * @param page
	 * @return
	 * @throws SolrServerException
	 * @throws IOException
	 */
	public static WebResponse search(String field, String search, int page) throws SolrServerException, IOException {
		SolrQuery solrQuery = new SolrQuery();
		solrQuery.setRequestHandler("/select");

		if (field.equals("vote_count")) {
			int top = Integer.parseInt(search) + 10;
			search = "[" + search + " TO " + top + "]";
		}

		solrQuery.setQuery(field + ":" + search);
		solrQuery.set("wt", "json");

		// If page > 0, only 3 results are returned from the page selected
		if (page > 0) {
			int pageTemp = page - 1;
			int skip = pageTemp * 3;
			solrQuery.set("rows", 3);
			solrQuery.set("start", skip);
		}

		QueryResponse qr = searchClient.query(solrQuery);
		WebResponse wr = new WebResponse(qr);
		return wr;
	}

	/**
	 * Search for web (input text)
	 * 
	 * @param webSearch
	 * @param page
	 * @return
	 * @throws SolrServerException
	 * @throws IOException
	 */
	public static WebResponse webSearch(String webSearch, int page) throws SolrServerException, IOException {
		return search("web_search", webSearch, page);
	}

	/**
	 * Search for facets on a single field
	 * 
	 * @param field
	 * @return
	 * @throws Exception
	 */
	public static List<Count> searchFacet(String field) throws Exception {
		SolrQuery query = new SolrQuery();
		query.setRequestHandler("/select");
		query.set("rows", 0);
		query.setQuery("*:*");
		query.setFacet(true);
		query.addFacetField(field);

		QueryResponse qr = searchClient.query(query);
		FacetField facet = qr.getFacetField(field);

		return facet.getValues();
	}

	/**
	 * Search range facet for a field
	 * 
	 * @param field
	 * @param gap
	 * @param start
	 * @param end
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public static List<RangeFacet> searchRangeFacet(String field, int gap, int start, int end) throws Exception {
		SolrQuery query = new SolrQuery();
		query.setRequestHandler("/select");
		query.set("rows", 0);
		query.setQuery("*:*");
		query.setFacet(true);

		query.addNumericRangeFacet(field, start, end, gap);

		QueryResponse qr = searchClient.query(query);
		return qr.getFacetRanges();
	}

	/**
	 * Search for a suggestion with the spellchecker
	 * 
	 * @param text
	 * @return
	 * @throws Exception
	 */
	public static List<Suggestion> searchSpellings(String text) throws Exception {
		resetSpellClient();
		SolrQuery query = new SolrQuery();
		query.setRequestHandler("/spellEdureka");
		query.set("spellcheck.reload", true);
		if (!spellIndexBuilt) {
			query.set("spellcheck.build", true);
		}
		query.set("spellcheck.q", text);
		query.set("spellcheck", true);
		query.set("wt", "json");

		try {
			QueryResponse qr = spellClient.query(query);
			spellIndexBuilt = true;
			SpellCheckResponse sr = qr.getSpellCheckResponse();
			return sr.getSuggestions();
		} catch (Exception e) {
			// e.printStackTrace();
			System.out.println("Spell blocked? ... skipping.");
		} finally {
			query.clear();
			spellClient.close();
		}
		return null;
	}
}
