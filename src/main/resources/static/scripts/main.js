/**
 * This scripts enable to show the suggest functionality Tries to connect the
 * server at an specific endpoint to get the suggested results
 */

var currentPage = 1;
var lastValue = ""
var lastField = ""

/**
 * This function performs the searching against the server
 */
function search(page, value, field) {
	$("#spellingPanel").hide();
	$("#spellingPanelWords").hide();
	hideSuggestionPanel();
	value = value.toLowerCase();
	currentPage = page;
	urlTarget = "";
	
	if (field != null) {
		urlTarget = "/search/" + value + "/" + page + "/" + field;
	} else {
		urlTarget = "/search/" + value + "/" + page + "/null";
	}
	
	if (value != null) {
		lastField = field;
		lastValue = value;
		$.ajax({
			url : urlTarget,
			success : showResults,
			error : resetResults
		});
		
		// Search suggestions (spell)
		$.ajax({
			url : "/spell/" + value,
			success : showSpellings,
			error : function(){}
		});
	}
}

/**
 * Show the results panel and fill it with the search result
 * 
 * @returns
 */
function showResults(data) {
// console.log(JSON.parse(data));
	resetResults();
	try {
		var parsedResults = JSON.parse(data);
		showPagination(parsedResults);
		if (parsedResults.results && parsedResults.results.length > 0) {
			var results = JSON.parse(parsedResults.results);
			for (var i = 0; i < results.length; i++) {
				$("#resultsPanel").append(createResult(results[i]));
			}
		}
	} catch (e) {
		console.log(e);
	}
}

/**
 * Shows the pagination panel and updates it
 * 
 * @param data
 * @returns
 */
function showPagination(parsedResults) {
	$("#paginationPanelItems").empty();
	if (parsedResults.numFound > 0) {
		$("#paginationPanel").fadeIn();
		// Calculate pages
		var numPages = parsedResults.numFound / 3;
		numPages = Math.ceil(numPages);
		for (var i = 0; i < numPages; i++) {
			$("#paginationPanelItems").append(createPaginationItem((i + 1)));
		}
	}
}

/**
 * Creates a pagination item and binds a click event to search the desired page
 * 
 * @param number
 * @returns
 */
function createPaginationItem(number) {
	var div = $("<div>");
	if (number == currentPage) {
		$(div).addClass("paginationItemCurrent");
	} else {
		$(div).addClass("paginationItem");
	}
	$(div).text(number);
	$(div).click(function() {
		search(number, lastValue, lastField);
	});
	return div;
}

/**
 * Creates a div that represents a search result
 * 
 * @param resultData
 * @returns
 */
function createResult(resultData) {
	var panel = $("<div>");
	$(panel).addClass("panel panel-primary");

	var panelHeading = $("<div>");
	$(panelHeading).addClass("panel-heading");
	$(panelHeading).text(resultData.title);

	var panelBody = $("<div>");
	$(panelBody).addClass("panel-body");

	var tagline = $("<p>");
	$(tagline).text(resultData.tagline);

	var genre = $("<p>");
	$(genre).text("Genre: " + resultData.genre);

	var keyWords = $("<p>");
	$(keyWords).text("Keywords: " + resultData.keywords);

	var voteCount = $("<p>");
	$(voteCount).text("Votes: " + resultData.vote_count);

	var languages = $("<p>");
	$(languages).text("Languages: " + resultData.original_languages);

	$(panelBody).append(tagline);
	$(panelBody).append(genre);
	$(panelBody).append(voteCount);
	$(panelBody).append(keyWords);
	$(panelBody).append(languages);

	$(panel).append(panelHeading);
	$(panel).append(panelBody);

	return panel;
}

/**
 * Gets language and vote count facets
 * 
 * @returns
 */
function getFacets() {
	$.ajax({
		url : "/facets/original_languages",
		success : setLanguagesFacets,
		error : function() {
		}
	});

	$.ajax({
		url : "/facets/vote_count",
		success : setVoteCountFacets,
		error : function() {
		}
	});
}

/**
 * Sets the language facets
 * 
 * @returns
 */
function setLanguagesFacets(data) {
	$("#languageFacetsPanelList").empty();
	try {
		var data = JSON.parse(data);
// console.log(data);
		for ( var key in data) {
			$("#languageFacetsPanelList").append(
					createFacetElement(key, data[key], true));
		}
	} catch (e) {
		console.log(e);
	}

}

/**
 * Sets the votes facets
 * 
 * @returns
 */
function setVoteCountFacets(data) {
	$("#votesFacetsPanelList").empty();
	try {
		var data = JSON.parse(data);
// console.log(data);
		for ( var key in data) {
			$("#votesFacetsPanelList").append(
					createFacetElement(key, data[key], false));
		}
	} catch (e) {
		console.log(e);
	}
}

function createFacetElement(value, count, isLanguageFacet) {
	var div = $("<div>");

	if (isLanguageFacet) {
		$(div).text(value + " (" + count + ")");
		$(div).click(function() {
			search(1, value, "original_languages");
		});
	} else {
		$(div).text((value -10) + "-" + value + " (" + count + ")");
		$(div).click(function() {
			search(1, value, "vote_count");
		});
	}

	$(div).addClass("facetElement");

	return div;
}

/**
 * Empty the results panel
 * 
 * @returns
 */
function resetResults() {
	$("#resultsPanel").empty();
}

/**
 * Sets the suggestions panel
 * 
 * @returns
 */
function prepareSuggestionsPanel() {
	panelWidth = $("#searchInput").width();
	$("#suggestionPanel").width(panelWidth);
}

/**
 * This function is triggered on case a key is pressed If the user press enter,
 * makes the search with the text typed in the control
 */
function suggest(input, event) {
	// If user press enter, search whatever be in the input
	if (event.keyCode == 13) {
		search(1, $("#searchInput").val(), null);
		return;
	}

	// If there's no value for search, exit
	if (input.value != null) {
		if (input.value.length > 2) {
			$.ajax({
				url : "/suggest/" + input.value,
				success : showSuggestions,
				error : hideSuggestionPanel
			});
		} else {
			hideSuggestionPanel();
		}
	}
}

/**
 * Shows the response in the panel
 * 
 * @param data
 * @returns
 */
function showSuggestions(data) {
// console.log("Result of suggestions: " + data);
	responseJson = JSON.parse(data);
	$("#suggestionPanel").empty();
	if (responseJson.suggestions && responseJson.suggestions.length > 0) {
		for (var i = 0; i < responseJson.suggestions.length; i++) {
			$("#suggestionPanel").append(
					constructSuggestion(responseJson.suggestions[i]));
		}
		$("#suggestionPanel").fadeIn();
	}

}

/**
 * Construct a suggestion div with the text on it and propper events
 * 
 * @param text
 * @returns
 */
function constructSuggestion(suggestionText) {
	var suggestItem = $("<div>");
	$(suggestItem).text(suggestionText);
	$(suggestItem).addClass("suggestionItem");
	$(suggestItem).click(function() {
		selectSuggestion(suggestionText);
	});
	return suggestItem;
}

/**
 * Replace the search value with the suggested one Hides the panel Triggers
 * search
 * 
 * @param suggestion
 * @returns
 */
function selectSuggestion(suggestion) {
// console.log(suggestion);
	$("#searchInput").val(suggestion);
	hideSuggestionPanel();
	search(1, $("#searchInput").val(), null);
}

/**
 * Hides the suggestions panel
 * 
 * @returns
 */
function hideSuggestionPanel() {
	$("#suggestionPanel").hide();
}

/**
 * Search for a spelling (only works for ocean/oceans)
 * 
 * @param text
 * @returns
 */
function showSpellings(data) {
	try {
		var spellings = JSON.parse(data);
		var suggestions = "";
		var word = ""
		for (key in spellings) {
			word = key;
			suggestions += spellings[key] + " ";
		}
		if (word != null && suggestions != null && suggestions.length > 0) {
			$("#spellingPanel").text(word + "?... may be you want to search: ");
			$("#spellingPanelWords").text(suggestions);
			$("#spellingPanel").fadeIn();
			$("#spellingPanelWords").fadeIn();
			console.log(suggestions);
			$("#spellingContainer").click(function(){
				search(1, suggestions.split(" ")[0], null);
			});
		}
	} catch (e) {
		console.log(e);
	}
}

/**
 * Init function
 * 
 * @returns
 */
$().ready(function() {
	getFacets();
	$("#searchButton").click(function() {
		search(1, $("#searchInput").val(), null);
	});
});